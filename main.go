package main

import (
	"flag"
	"github.com/aaoiki/margopher"
	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
	"log"
	"strings"
	"time"
)

func main() {
	consumerKey := flag.String("consumer-key", "", "Twitter Consumer Key")
	consumerSecret := flag.String("consumer-secret", "", "Twitter Consumer Secret")
	accessToken := flag.String("access-token", "", "Twitter Access Token")
	accessSecret := flag.String("access-secret", "", "Twitter Access Secret")
	interval := flag.Int("interval", 30, "Interval between tweets in minutes")
	minwords := flag.Int("minwords", 5, "Minimum amount of words to tweet")
	filepath := flag.String("filepath", "ks.log", "Path to the log file to base markov chain on")
	flag.Parse()

	if *consumerKey == "" || *consumerSecret == "" || *accessToken == "" || *accessSecret == "" {
		log.Fatal("Consumer key/secret and Access token/secret required")
	}

	config := oauth1.NewConfig(*consumerKey, *consumerSecret)
	token := oauth1.NewToken(*accessToken, *accessSecret)
	m := margopher.New()
	httpClient := config.Client(oauth1.NoContext, token)
	client := twitter.NewClient(httpClient)

	ticker := time.NewTicker(time.Minute * time.Duration(*interval))

	for range ticker.C {
		status := strings.Split(m.ReadFile(*filepath), "\n")[0]
		for len(strings.Split(status, " ")) < *minwords {
			status = strings.Split(m.ReadFile(*filepath), "\n")[0]
		}

		tweet, _, err := client.Statuses.Update(status, nil)
		if err != nil {
			log.Printf("[ERROR] %s\n", err.Error())
		} else {
			log.Printf("[SUCCESS] Tweeted \"%s\"\n", tweet.Text)
		}
	}
}
